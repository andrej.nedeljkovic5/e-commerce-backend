package rest_api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rest_api.model.User;

@RestController
@CrossOrigin
@RequestMapping(path = "webapp/api/user")
public class UserController {
    
    private List<User> userList = new ArrayList<>();
    
    public UserController () {

    	userList.add(new User(2, "Harry Potter", "harry@gmail.com", "12345"));
    	userList.add(new User(3, "Mike Tyson", "mike@gmail.com", "12345"));
    	userList.add(new User(4, "Andrew Tate", "andrewt@gmail.com", "12345"));
    	userList.add(new User(5, "Hermione Granger", "hermione@gmail.com", "12345"));
    	userList.add(new User(6, "Dave Chappelle", "dave@gmail.com", "12345"));
    }

    @GetMapping("/findall")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> retVal = userList;
        return new ResponseEntity<>(retVal, HttpStatus.OK);
    }

    @GetMapping("/findbyid/{id}")
    public ResponseEntity<User> geUserById(@PathVariable long id) {
        User retVal = null;
        for (User user: userList) {
            if (user.getId() == id) {
            	retVal = user;
            }
        }
        if (retVal != null) {
            return new ResponseEntity<>(retVal, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/save")
    public ResponseEntity<String> addUser(@RequestBody User user) {
    	User existingUser = null;
    	for (User currentUser: userList) {
            if (currentUser.getId() == user.getId()) {
            	existingUser = user;
            }
        }
    	
    	if (existingUser == null) {
    		userList.add(user);
    		return new ResponseEntity<>("New user successfully added.", HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Error while adding user, user with that id already exists", HttpStatus.BAD_REQUEST);
    	}
    }
    
    @PutMapping("/edit/{id}")
    public ResponseEntity<String> editUser(@PathVariable long id, @RequestBody User user) {
    	User retVal = null;
        for (User currentUser: userList) {
            if (currentUser.getId() == id) {
            	retVal = user;
            }
        }
        if (retVal != null) {
        	int intId = (int) id;
        	getById(intId).setFullName(retVal.getFullName());
        	getById(intId).setEmail(retVal.getEmail());
        	getById(intId).setPassword(retVal.getPassword());
        	
            return new ResponseEntity<>("User edited successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable long id) {
        for (User user: userList) {
            if (user.getId() == id) {
                userList.remove(user);
                return new ResponseEntity<>("User successfully deleted.", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("User not found.", HttpStatus.NOT_FOUND);
    }
    
    private User getById(int id) {
    	for (User user : userList) {
			if (user.getId() == id) {
				return user;
			}
		}
    	return null;
    }
}
