package rest_api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rest_api.model.Product;

@RestController
@CrossOrigin
@RequestMapping(path = "webapp/api/product")
public class ProductController {
	
	private List<Product> productList = new ArrayList<>();
    
    public ProductController () {
    	productList.add(new Product(1, "Iphone", "Iphone", 3400));
    	productList.add(new Product(2, "Iphone", "Iphone", 80000));
    	productList.add(new Product(3, "Iphone", "Iphone", 55000));
    	productList.add(new Product(4, "Iphone", "Iphone", 45000));
    	productList.add(new Product(5, "Iphone", "Iphone", 99000));
    	productList.add(new Product(6, "Iphone", "Iphone", 40000));
    }

    @GetMapping("/findall")
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> retVal = productList;
        return new ResponseEntity<>(retVal, HttpStatus.OK);
    }

    @GetMapping("/findbyid/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable long id) {
    	Product retVal = null;
        for (Product product: productList) {
            if (product.getId() == id) {
            	retVal = product;
            }
        }
        if (retVal != null) {
            return new ResponseEntity<>(retVal, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/save")
    public ResponseEntity<String> addProduct(@RequestBody Product product) {
    	Product existingProduct = null;
    	for (Product currentProduct: productList) {
            if (currentProduct.getId() == product.getId()) {
            	existingProduct = product;
            }
        }
    	
    	if (existingProduct == null) {
    		productList.add(product);
    		return new ResponseEntity<>("New product successfully added.", HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Error while adding product, product with that id already exists", HttpStatus.BAD_REQUEST);
    	}
    }
    
    @PutMapping("/edit/{id}")
    public ResponseEntity<String> editProduct(@PathVariable long id, @RequestBody Product product) {
    	Product retVal = null;
        for (Product currentProduct: productList) {
            if (currentProduct.getId() == id) {
            	retVal = product;
            }
        }
        if (retVal != null) {
        	int intId = (int) id;
        	getById(intId).setName(retVal.getName());
        	getById(intId).setDescription(retVal.getDescription());
        	getById(intId).setPrice(retVal.getPrice());
        	
            return new ResponseEntity<>("Product edited successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable long id) {
        for (Product product: productList) {
            if (product.getId() == id) {
            	productList.remove(product);
                return new ResponseEntity<>("Product successfully deleted.", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Product not found.", HttpStatus.NOT_FOUND);
    }
    
    private Product getById(int id) {
    	for (Product product : productList) {
			if (product.getId() == id) {
				return product;
			}
		}
    	return null;
    }
}
